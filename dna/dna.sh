#!/opt/local/bin/bash
# Solution to Rosalind Problem 1
#

declare -A array

while IFS= read -r -n1 char; do
	echo "char:[${#char}]"
	[[ "${#char}" -gt 0 ]] &&
	array[$char]=$((${array[$char]}+1))
done < ${1:-rosalind_dna.txt}

for key in ${array[@]}; do
	echo -n "$key "
done
echo
