#!/bin/bash
# Answer to Rosalind Problem 3
echo $(which tac)
rev=$(tac ${1:-test_revc.txt})
rev=${rev//A/T}
rev=${rev//C/G}
echo $rev
